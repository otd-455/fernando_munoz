# PRUEBA VY UI

## Instrucciones

Maqueta la imagen base.png
![base.png](base.png)

### Considera:
- Usa las imágenes incluidas en /images
- Usa los textos proporcionados en text.txt
- Ha de ser responsive
- No se puede usar bootstrap o librerías de estilos
- Utiliza Material Design Icons para los iconos (ya incluido)
- Se puede implementar en css directamente (styles.css) o usando sass (styles/**.scss). Usa `npm run dev` para compilar los estilos en tiempo real o `npm run build` para compilarlos una sola vez
- Se valora el uso de propiedades de accesibilidad
- Se valora semantica y estructura
- Se ha de ver correctamente en **IE(10 y 11), Chrome, Firefox, Opera y Safari**


### Instrucciones para usar sass
- El uso de sass es opcional
- Debes tener [`node`](https://nodejs.org/en/download/) instalado
- Usa `npm install` para instalar las dependencias de compilación
- Usa `npm run dev` para compilar los estilos cada vez que se detecte un cambio.
- Usa `npm run build` para compilar los estilos una sola vez

**Por favor, el comentario del commit final ha de ser "Finished", para informarnos de que se ha finalizado la prueba.**